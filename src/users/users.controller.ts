import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Controller('users')
export class UsersController {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {}

    @Get()
    async findAll(): Promise<User[]> {
        return await this.userRepository.find();
    }

    @Post()
    async create(@Body() user: User): Promise<User> {
        return await this.userRepository.save(user);
    }

    /* @Get(':id')
    async findOne(@Param('id') id: number): Promise<User> {
        return await this.userRepository.findOne(id);
    } */

    @Put(':id')
    async update(@Param('id') id: number, @Body() user: User): Promise<void> {
        await this.userRepository.update(id, user);
    }

    @Delete(':id')
    async remove(@Param('id') id: number): Promise<void> {
        await this.userRepository.delete(id);
    }
}
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Video } from '../videos/video.entity';

@Entity()
export class Statistics {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  views: number;

  @Column()
  streams: number;

  @ManyToOne(type => Video, video => video.statistics)
  video: Video;
}
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { User } from '../users/user.entity';
import { Statistics } from '../statistics/statistics.entity';

@Entity()
export class Video {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  path: string;

  @ManyToOne(type => User, user => user.videos)
  uploader: User;

  @OneToMany(type => Statistics, statistics => statistics.video)
  statistics: Statistics[];
}